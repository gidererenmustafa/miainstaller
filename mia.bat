TXT="MiaUserBot Otomatik Deploy Kuruluma Hoş Geldiniz"
TXT+="\nTelegram: @MiaUserBot"
pkg update -y
rm -rf miainstaller
clear
echo -e $TXT
echo "Python Yükleniyor ⌛"
pkg install python -y
clear
echo -e $TXT
echo "Git Yükleniyor ⌛"
pkg install git -y
clear
echo -e $TXT
echo "Pyrogram Yükleniyor ⌛"
pip install pyrogram tgcrypto
echo "Repo klonlanıyor... ⌛"
git clone https://github.com/MiaUserBot/miainstaller
clear
echo -e $TXT
cd miainstaller
clear
echo "Gereksinimler Yükleniyor ⌛"
echo -e $TXT
pip install -r requirements.txt
clear
python -m mia_installer
